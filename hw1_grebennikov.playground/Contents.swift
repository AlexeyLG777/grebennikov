import UIKit

                        // 1. Константы, переменные и типы данных

//1
let aInt: Int = -1  // var aInt: Int = -1
print("Максимальное значение типа Int: \(Int.max), минимальное значение типа Int: \(Int.min)")

let aUInt: UInt = 1  // var aUInt: UInt = 1
print("Максимальное значение типа UInt: \(UInt.max), минимальное значение типа UInt: \(UInt.min)")

let aFloat: Float = 3.141592653589793238462643 // var aFloat: Float = -2,233123
print(aFloat)

let aDouble: Double = 3.141592653589793238462643 // var aDouble: Double = -2.421341232356748
print(aDouble)

let name: String = "Alexey"
var car: String = "Volvo"

//2
typealias ProductName = String
let fruit: ProductName = "Lemon"
let vegetables: ProductName = "Cucumber"
let bread: ProductName = "Narochansky"

//3
let aa = 5
let ab = 5.5

let result = Double(aa) + ab

//4
let firstItem = 6
let secondItem = 2

print("\(firstItem) + \(secondItem) = \(firstItem + secondItem)")
print("\(firstItem) - \(secondItem) = \(firstItem - secondItem)")
print("\(firstItem) * \(secondItem) = \(firstItem * secondItem)")
print("\(firstItem) / \(secondItem) = \(firstItem / secondItem)")

//5
let a: Double = 3.14
let b: Double = 8.68

let average = (a + b) / 2

//6
let fahrenheit = 80.5

let celsius = (fahrenheit - 32) / 1.8
print(celsius)

//7
let degrees = 58.22

let pi = 3.141592653589793238462643
let radians = 2 * pi / 360

//8
let x1 = 2.88
let y1 = 3.77
let x2 = 6.86
let y2 = 4.31

let distance = sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)))




                                //2. Строки

//1
let myName = "Alexey"
let mySurname = "Grebennikov"
var age = 35
var location = "Minsk"
var hobby = "Autosport"

//2
print("Меня зовут \(myName), фамилия: \(mySurname). Мне \(age) лет, живу в \(location) и увлекаюсь \(hobby)")

//3
let college = "Minskiy tehnikum predprinimatelstva"
let collegeEnd = 2003
let institute = "Minskiy institut upravleniya"
let instituteEnd = 2007
let myFirstSоnBorn = 2014
let myFirstSonName = "Yaroslav"
let mySecondSonBorn = 2017
let mySecondSonName = "Vladislav"
let myHobby = "Extreme driving"
let yearOfEndCourses = 2019

let aboutMe1 = "Я учился в \(college), закончил в \(collegeEnd). \nЗатем поступил в \(institute) и закончил его в \(instituteEnd). \nВ \(myFirstSоnBorn) у меня родился сын \(myFirstSonName) и в \(mySecondSonBorn) у меня родился еще сын по имени \(mySecondSonName). \nЯ увлекаюсь \(myHobby) и в \(yearOfEndCourses) я закончил курсы экстремального вождения"
print(aboutMe1)

//4
print("A \nL \nE \nX \nE \nY")

let myFirstName = "Alexey"
for i in myFirstName {
    print(i)
}

//5
var myFavoriteNumber = 7
var numberDescribe = "Любимое число"

let aboutMe = "\(numberDescribe): \(String(myFavoriteNumber))"

//6
let firstName = "Alexey"
let lastName = "Grebennikov"

//7
let fullName = "\(firstName) \(lastName)"

//8
let myDetails = "Hello, my name is \(fullName)"

                        //3. Tuples
//1 проверить
let student: (String, Int, String) = ("Alexey", 35, "Minsk")
print(student)
print(student.0, student.1, student.2)

//2
let GAIToDoList1: (speed: Int, drunk: Int, noLicense: Int) = (15, 3, 7)
print(GAIToDoList1)

//3

//4
let GAIToDoList2: (speed: Int, drunk: Int, noLicense: Int) = (10, 2, 4)
print(GAIToDoList2)

//5
let GaiToDoListCompare: (speed: Int, drunk: Int, noLicense: Int) = ((GAIToDoList1.speed - GAIToDoList2.speed), (GAIToDoList1.drunk - GAIToDoList2.drunk), (GAIToDoList1.noLicense - GAIToDoList2.noLicense))
print(GaiToDoListCompare)

//6
var month: Int = 6
var day: Int = 30
var year: Int = 2020
var averageTemperature: Double = 19.6

let tuple = (month, day, year, averageTemperature)
// let tuple = (month: month, day: day, year: year, averageTemperature: averageTemperature)

//7
var tupleTempOnDate = (month: month, day: day, year: year, averageTemperature: averageTemperature)

//8


//9

// ps доделаю еще, я со второго занятия, поэтому трудно успеть( не могу понять как выводить тремя способами в консоль

